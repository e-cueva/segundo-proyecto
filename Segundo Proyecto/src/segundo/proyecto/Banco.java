package segundo.proyecto;

import java.util.Scanner;

public class Banco {

    private String nombre;
    private String apellido;
    private int cedula;
    private int nmcuenta;
    Scanner data = new Scanner(System.in);

    public Banco(String no, String ap, int id, int nc) {
        this.nombre = no;
        this.apellido = ap;
        this.cedula = id;
        this.nmcuenta = nc;
    }

    public String getNombre() {
        
        
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
       
        
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getCedula() {
        System.out.println("Ingrese identificacion de Usuario");
        cedula = data.nextInt();
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public int getNmcuenta() {
        System.out.println("Ingrese numero de cuenta");
        nmcuenta = data.nextInt();
        return nmcuenta;
    }

    public void setNmcuenta(int nmcuenta) {
        this.nmcuenta = nmcuenta;
    }

}
