package segundo.proyecto;

public class Usuario extends Banco {
    
    public Usuario(String no, String ap, int id, int cc) {
        super(no, ap, id, cc);
    }

    @Override
    public String toString() {
        
        String p = ("\n NOMBRE : " + getNombre() + "\n APELLIDO : " + getApellido() + "\n CEDULA :" + getCedula() + "\n NUMERO DE CUENTA : " + getNmcuenta());
        System.out.println("*************************************");
        return p;
    }

}
